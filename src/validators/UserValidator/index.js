import { body, param } from 'express-validator'

class UserValidator {
  static create = () => [
    body('email').exists().isString(),
    body('password').exists().isString(),
    body('user_name').exists().isString()
  ]

  static update = () => [
    param('id').exists().isUUID(),
    body('email').optional().isString(),
    body('password').optional().isString(),
    body('user_name').optional().isString()
  ]

  static delete = () => [
    param('id').exists().isUUID(),
  ]
}

export default UserValidator