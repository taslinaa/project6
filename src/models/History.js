import { Model } from 'sequelize';

module.exports = (sequelize, DataTypes) => {
  class History extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      const { User } = models

      History.belongsTo(User, { foreignKey: 'userId', as: 'userInfo' })
    }
  };
  History.init({
    id: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    score: DataTypes.STRING,
    time: DataTypes.DATE,
    userId: DataTypes.UUID
  }, {
    sequelize,
    modelName: 'History',
    timestamps: true
  });
  return History;
};


