import { Op } from 'sequelize'
import {
  History, User, sequelize,
} from '../../models'

class HistoryController {
  static get = (req, res) => {
    const {
      query = '', sort = 'asc', page = 1, itemPerPage = 10,
    } = req.query

    return History.findAll({
      attributes: ['id', 'score', 'time'],
      where: {
        [Op.or]: [
          { score: { [Op.iLike]: `%${query}%` } }
        ],
      },
      order: [
        ['score', sort],
      ],
      limit: itemPerPage,
      offset: (page - 1) * itemPerPage,
      include: [
        {
          model: User,
          as: 'userInfo',
          attributes: ['user_name'],
        },
      ],
    }).then(
      (posts) => res.status(200).json(posts),
    ).catch(
      (e) => console.log(e),
    )
  }

  static getIndexView = (req, res) => {
    res.render(
      'history',
      { user: 'taslina' },
    )
  }

  static create = (req, res) => {
    const {
      score, time, userId
    } = req.body

    return History.create({
      score,
      time,
      userId
    }).then ( (history) => 
      res.status(201).json({history}),
    ).catch(
      (e) => console.log(e),
    )
  }

  static update = (req, res) => {
    const { id } = req.params

    return History.findOne({
      where: { id },
    }).then(
      (history) => {
        if (!history) return res.status(404).json({ message: 'Not found' })

        const { score, time } = req.body

        return history.update(
          { score, time },
        ).then(
          (updated) => res.status(200).json({ ...updated.dataValues }),
        )
      },
    ).catch(
      (e) => console.log(e),
    )
  }

  static delete = (req, res) => {
    const { id } = req.params

    return History.destroy({
      where: { id },
    }).then(
      (history) => {
        if (!history) return res.status(404).json({ message: 'Not found' })

        return res.status(200).json({ message: 'Deleted' })
      },
    ).catch(
      (e) => console.log(e),
    )
  }
}

export default HistoryController